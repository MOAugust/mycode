package MyActor

import akka.actor._
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.mutable

/**
 * 额... 尽量一次性用maven导包的时候写对(查看akka与scala对照)
 * 我折腾了挺久
 */
class MyResourceManager(var resourceMangerHostName: String,var resourceManagerPort: Int) extends Actor{
  /**
   * 定义一个map ,接受MyNodeManager 的注册信息
   * key 是主机名 ;value是NodeManagerInfo对象,
   * 里边储存主机名,CPU,内存信息
   */
  var registerMap = new mutable.HashMap[String, NodeManagerInfo]()
  
  /**
   * 定义一个Set,接受MyNodeNanager的注册信息,
   * key是主机名,value是NodeManagerInfo对象,
   * 里边储存主机名,CPU,内存信息
   * 实际 这个和map 中存档内容是一致的,容易遍历;此处是模仿Spark中的内容
   * 便于理解spark源码
   */
  var registerSet = new mutable.HashSet[NodeManagerInfo]()
  
  override def preStart(): Unit = {
    import scala.concurrent.duration._
    import context.dispatcher
    context.system.scheduler.schedule(0 millis, 5000 millis, self, CheckTimeOut)
  }
  
  
  //对MyNodeManager传过来的信息进行匹配
  override def receive: Receive = {
    //1.匹配到的NodeManager的注册信息进行对应处理
    case NodeManagerRegisterMsg(nodeManagerID, cpu, memory) => {
      //将注册信息实例化为一个NoderManagerInfo对象
      val registerMsg = new NodeManagerInfo(nodeManagerID, cpu, memory)
      /**
       * 将注册信息储存到registerMap和registerSet里边,
       * key是主机名,value 是NodeManagerInfo对象
       */
      registerMap.put(nodeManagerID, registerMsg)
      registerSet += registerMsg
      //注册成功后,反馈MyNodeManager一个成功的信息
      sender() ! new RegisterFeedbackMsg("你注册成功了! "+
          resourceMangerHostName+":"+resourceManagerPort)
    }
    
    //2.匹配到心跳信息做相应处理
    case HeartBeat(nodeManagerID) => {
      //获取当前时间
      val time: Long = System.currentTimeMillis()
      //根据nodeManagerID获取NodeManagerInfo对象
      val info = registerMap(nodeManagerID)
      info.lastHeartBeatTime = time
      /**
       * 更新registerMap和registerSet里的nodeManager
       * 对应的NodeManagerInfo对象信息(最后一次心跳时间)
       */
      registerMap(nodeManagerID) = info
      registerSet += info
    }
    
    //3.检测超时,对超时的数据从集合中删除
    case CheckTimeOut => {
      var time = System.currentTimeMillis()
      registerSet
        .filter(nm => time - nm.lastHeartBeatTime > 10000)
        .foreach(deadnm => {
          registerSet -= deadnm
          registerMap.remove(deadnm.nodeManagerID)
        })
        println("当前注册成功节点数: "+ registerMap.size)
       
    }

  }
  
}


object MyResourceManager {
  def main(args: Array[String]): Unit = {
    /**
     * 传参:
     * ResourceManager的主机地址,端口号
     */
    val RM_HOSTNAME = args(0)
    val RM_PORT = args(1).toInt
        
    val str: String = 
      """
         |akka.actor.provider = "akka.remote.RemoteActorRefProvider"
         |akka.remote.netty.tcp.hostname =localhost
         |akka.remote.netty.tcp.port=19888
      """.stripMargin
      
    val conf: Config = ConfigFactory.parseString(str)
    val actorSystem = ActorSystem(Conf.RMAS, conf)
    actorSystem.actorOf(Props(new MyResourceManager(RM_HOSTNAME, RM_PORT)), Conf.RMA)
  }
}