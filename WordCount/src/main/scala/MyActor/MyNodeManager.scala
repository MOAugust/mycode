package MyActor

import akka.actor._
import java.util.UUID
import com.typesafe.config.{Config, ConfigFactory}



class MyNodeManager(resourceManagerHostName: String, resourceManagerPort: Int, cpu: Int,memory: Int) extends Actor{
  //MyNodeManager的UUID
  var nodeManagerID: String = _
  var rmref: ActorSelection = _
  override def preStart(): Unit = {
    //获取MyResourceManager的Actor的引用
    rmref = context.actorSelection(s"akka.tcp://${Conf.RMAS}@${resourceManagerHostName}:${resourceManagerPort}/user/${Conf.RMA}")
    //生成随机的UUID
    nodeManagerID = UUID.randomUUID().toString()
    /**
     * 向MyResourceManager发送注册信息
     */
    rmref ! NodeManagerRegisterMsg(nodeManagerID, cpu, memory)
  }
  
  //进行信息匹配
  override def receive: Receive = {
    
    //1.匹配到注册成功之后MyResourceManager 反馈回的信息,进行相应处理
    case RegisterFeedbackMsg(feedbackMsg) => {
      /**
       * initialDelay: FiniteDuration, 多久以后开始执行
       * interval:     FiniteDuration, 每隔多长时间执行一次
       * receiver:     ActorRef, 给谁发这个消息
       * message:      Any 发送的消息是什么
       */
      //定时任务需要导入工具包
      import scala.concurrent.duration._
      import context.dispatcher
      //定时向自己发送消息
      context.system.scheduler.schedule(0 millis, 3000 millis, self, SendMessage)
    }
    
    //2.匹配到SendMesszge信息之后做处理
    case SendMessage => {
      //向MyResourceManager发送心跳信息
      rmref ! HeartBeat(nodeManagerID)
      println(Thread.currentThread().getId + ":" + System.currentTimeMillis())
    }  
  }
}


object MyNodeManager {
  def main(args: Array[String]): Unit = {
    /**
     * 传参:
     *   NodeManager的主机地址,端口号,CPU,内存
     *   ResourceManager的主机地址,端口号
     */
    val NM_HOSTNAME = args(0)
    val NM_PORT = args(1)
    val NM_CPU: Int = args(2).toInt
    val NM_MEMORY: Int = args(3).toInt
    
    val RM_HOSTNAME = args(4)
    val RM_PORT: Int = args(5).toInt
    
    val str: String = 
      s"""
        |akka.actor.provider = "akka.remote.RemoteActorRefProvider"
        |akka.remote.netty.tcp.hostname = ${NM_HOSTNAME}
        |akka.remote.netty.tcp.port = ${NM_PORT}
       """.stripMargin
    val conf: Config = ConfigFactory.parseString(str) 
    val actorSystem = ActorSystem(Conf.NMAS, conf)
    actorSystem.actorOf(Props(new MyNodeManager(RM_HOSTNAME, RM_PORT, NM_CPU, NM_MEMORY)), Conf.NMA)
    
  }
}