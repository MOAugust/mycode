package dblab.ScalaDemo

object Currying {
  /**
   * 柯里化(currying, 以逻辑学家Haskell Brooks Curry的名字命名)
   * 指的是将原来接受两个参数的函数变成新的接受一个参数的函数的过程。
   * 新的函数返回一个以原有第二个参数作为参数的函数。
   * 在Scala中方法和函数有细微的差别，通常编译器会自动完成方法到函数的转换。
   */
  //Scala中柯里化方法的定义形式和普通方法类似，
  //区别在于柯里化方法拥有多组参数列表，每组参数用圆括号括起来
  def mysum(x: Int)(y: Int):Int = x +y
  /**
   * mysum方法拥有两组参数，分别是(x: Int)和(y: Int)
   * mysum方法对应的柯里化函数类型是：
   * Int => Int => Int
   * 柯里化函数是右结合的,即上边类型等价于
   * Int => (Int => Int)
   * 表明该函数若只接受一个Int参数，则返回一个Int => Int类型的函数
   */
  def main(args: Array[String]): Unit = {
    /**
     * 在Scala中可以直接操纵函数，但是不能直接操纵方法，
     * 所以在使用柯里化方法前，需要将其转换成柯里化函数
     */
    val f = mysum _
    val f1 = f(1)
    val f2 = f1(2)
    println(f+"\n"+f1+"\n"+f2)
  }
}