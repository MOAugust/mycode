package dblab.ScalaDemo

import scala.collection.mutable.ArrayBuffer

object ArrayDemo {
  def main(args: Array[String]): Unit = {
    /**
     * //数组
     * 如果是new,相当于调用了数组的apply方法
     * 直接为数组赋值
     */
    //初始化一个长度为8的数组,所有元素均为0
    val arr1 = new Array[Int](8)
    //直接打印定长数组,内容为数组的HashCode值
    println("arr1:"+arr1)
    //将数组转化为数组缓冲可以看到数组的内容
    //这里由toBuffer去实现
    println("arr1.toBuffer:"+arr1.toBuffer)
    
    //初始化一个长度为10的定长数组
    /**
     * 此处未new操作,故只是定义了定长数组,而未赋值
     */
    val arr2 = Array[Int](10)
    println("arr2:"+arr2)
    println("arr2.toBuffer:"+arr2.toBuffer)
    
    //定义一个长度为3的定长数组
    val arr3 = Array(11,22,33)
    //使用(下标)来访问,下标从0开始
    println("arr3(2):"+arr3(2))
    
   //------------------------------------
    //变长数组(数组缓冲)
    //变长数组使用数组缓冲,则需要导入包
    var ab = ArrayBuffer[Int]()
    println(ab)
    //想数组缓冲尾部追加元素
    //+= 尾部追加元素
    ab += 1
    ab += (2,3,8,5)
    //追加数组 ++=
    ab ++= Array(6,7)
    //追加数组缓冲  ++=
    ab ++= ArrayBuffer(4,9)
    //打印数组缓冲 ab
    println("追加 ab:"+ab)
    
    //在数组某个位置插入元素用 insert(索引,元素一个或多个)
    ab.insert(0, -1, 0)
    println("insert ab:"+ab)
    
    //删除数组某个元素用 remove(索引,删除元素个数)
    ab.remove(3, 2)
    
    println("remove ab:"+ab)
    
    /**
     * 遍历数组
     */
    //增强for循环
    for(i <- ab)
      print(i+"\t")
    println()
    //reverse是将前边的Range反转,range 列表
    for(i <- (0 until ab.length).reverse)
      print(ab(i)+"\t")
      println()
    /**
     * 数组转换 yield, 将原始数组进行转换成一个新数组,原数组不变
     */
    var ab2 = for(i <- ab if i % 2 == 0) yield i * 2
    //map 相当于将数组中每一个元素取出来应用传进去的函数
    //filter 过滤,接收返回值为boolean 的函数
    var ab3 = ab.map(_ * 2)
    var ab4 = ab.filter(_ % 2 == 0).map(_ * 10)
      println(ab2+"\n"+ab3+"\n"+ab4)
      //数组算法,求和,最大值,排序
      println(ab4.sum+"\n"+ab4.max+"\n"+ab4.sorted)
    
  }
}