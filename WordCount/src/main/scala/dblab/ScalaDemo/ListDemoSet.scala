package dblab.ScalaDemo

import scala.collection._

object ListDemoSet {
  def main(args: Array[String]): Unit = {
    //不可变set
    //创建一个空set
    val set1 = new immutable.HashSet[Int]()
    println("set1:"+set1)
    //将元素和set合并生成一个新的set,原set不变
    val set2 = set1 + 1
    println("set2:"+set2)
    //set中元素不重复
    //(是结果不会重复,而不是不能重复,相当于自动去重)
    val set3 = set2 ++ Set(1,2,3)
    println(set3)
    val set4 = Set(1,2,3,4) ++ set3
    println(set4)
    
    //可变set
    //创建可变的空set
    val set11 = new mutable.HashSet[Int]()
    println("set11:"+set11)
    //向set11中添加元素
    set11 += 1
    //add等价于 +=
    set11.add(2)
    set11 ++= Set(3,4,5)
    println("set11:"+set11)
    //删除一个元素
    set11 -= 5
    set11.remove(3)
    println("set11:"+set11)
  }
}