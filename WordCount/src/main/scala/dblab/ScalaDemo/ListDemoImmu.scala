package dblab.ScalaDemo

import scala.collection.immutable._
//不可变的序列 import scala.collection.immutable._
object ListDemoImmmu {
  def main(args: Array[String]): Unit = {
    /**
     * 在Scala中列表要么为空（Nil表示空列表）要么是一个head元素加上一个tail列表
     * 9 :: List(5, 2)  :: 操作符是将给定的头和尾创建一个新的列表
     * :: 操作符是右结合的，如9 :: 5 :: 2 :: Nil相当于 9 :: (5 :: (2 :: Nil))
     */
    //创建一个不可变集合
    val list1 = List(1,2,3)
    //将0插入集合list1前生成一个新集合
    val list2 = 0:: list1
    val list3 = list1.:: (0)
    val list4 = 0 +: list1
    val list5 = list1.+: (0)
    println(list1+"\n"+list2+"\n"+list3+"\n"+list4+"\n"+list5)
    
    //将4插入集合list1后生成一个新的集合
    val list6 = list1 :+ 4
    println(list6)
    
    //将两个集合合并为一个
    val list7 = list5 ++ list6
    println(list7)
    
    //将list6插入到list5前边生成一个新集合
    //反之 list5 ++: list6
    val list8 = list6 ++: list5
    println(list8)
  }
}