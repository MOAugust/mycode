package dblab.ScalaDemo

class GrandFather
class Father extends GrandFather
class Son extends Father
class Friend

object BoundsDemo {
  def getID[T >: Son](person: T): Unit = {
    println("Very Good!")
  }
  def main(args: Array[String]): Unit = {
   getID[GrandFather](new Father)
   getID[Father](new Son)
   getID[Son](new Son)
   //getID[Son](new Friend) 这条无法运行,因为它不是Son的父类
  }
}
