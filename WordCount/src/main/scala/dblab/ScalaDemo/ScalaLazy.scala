package dblab.ScalaDemo

object ScalaLazy {
  def init():String = {
    println("HHH 123")
    return "123"
  }
  def Linit():String = {
    println("HHH 123")
    return "123"
  }
  def main(args: Array[String]): Unit = {
    val name = init()
    //此处立刻调用init() 方法实例化
    println("HHH")
    println(name)
    println("-----")
    lazy val nam = Linit()
    println("HHH")
    //此处没有立刻实例化Linit(),而是在使用nam时,调用实例化方法
    //且无论做多少次调用,实例化方法只执行一次
    println(nam)
  }
}