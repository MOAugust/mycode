package dblab.ScalaDemo

object TupleDemo {
  def main(args: Array[String]): Unit = {
  //定义元组用小括号将元素包裹起来,元素间用逗号分隔
  //元素类型可以不一样,个数可以任意多个
  val tup = ("tuple", 1.23, 110)
  val tupl,(a, b, c) = ("tuple", 1.23, 110)
  //获取元组中的值,下划线加脚标,元组脚标从1开始
  val t1 = tup._1
  println(tup+"\n"+t1+"\n"+tupl)
  
  //toMap 可以将对偶的集合转换成映射
  val arr = Array(("aa", 10),("bb", 20),("cc", 30))
  println(arr.toBuffer)
  println(arr.toMap)
  //拉链操作
  //zip 命令可以将多个值绑定在一起
  val v = Array(1, 2, 3)
  val k = Array("a", "b", "c")
  println(k.zip(v).toBuffer)
  println(k.zip(v).toMap)
  
  //如果key或者value多出了呢
  val v1 = Array(1, 2, 3, 4, 5)
  val k1 = Array("a", "b", "c")
  println(k1.zip(v1).toBuffer)
  println(k1.zip(v1).toMap)
  
  val v2 = Array(1, 2, 3)
  val k2 = Array("a", "b", "c", "d", "e")
  println(k2.zip(v2).toBuffer)
  println(k2.zip(v2).toMap)
  /**
   * 结论:如果两个数组的元素个数不一致
   * 拉链操作后生成的数组的长度为较小的那个数组的元素个数
   */
   
  }
}