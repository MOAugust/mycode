package dblab.ScalaDemo

import scala.collection.mutable.Map

object MapDemo {
  def main(args: Array[String]): Unit = {
    //第一种 用箭头 ->创建Map
    val scores1 = Map("aa" -> 10, "bb" -> 20, "cc" -> 30)
    println(scores1)
    //第二种 用元组 ()创建Map
    val scores2 = Map(("aa",10),("bb",20),("cc",30))
    println(scores2)
    //返回映射值的两种方式
    println(scores1("bb"))
    println(scores1.get("bb"))
    //getOrElse：有就返回对应的值，没有就返回默认值
    println(scores1.getOrElse("dd", 40))
    /**
     * 注意：在Scala中，有两种Map，
     * 一个是immutable包下的Map，该Map中的内容不可变(默认为这个)
     * 另一个是mutable包下的Map，该Map中的内容可变 （val var）
     */
    val scores3 = Map("aa" -> 10,"bb" -> 20)
    println(scores3)
    //修改 Map 的内容
    scores3("aa") = 30
    println(scores3)
    //元素追加 +=
    scores3 += ("cc" -> 40, "dd" -> 50)
    println(scores3)
  }
}