package dblab.ScalaDemo

import scala.util.Random

case class Sub(id: String, name: String)
case class Heart(time: Long)
case object CheckTask


object ScalaClassDemo extends App {
  val arr = Array(CheckTask,Heart(147258),Sub("0001","task-0001"))
  val i = Random.nextInt(arr.length)
  println(i)
  arr(i) match {
    case Sub(id, name) => {
      println(s"$id, $name")
    }
    
    case Heart(time) => {
      println(time)
    }
    
    case CheckTask => {
      println("Check!")
    }
  }
}