package dblab.ScalaDemo

import scala.collection.mutable._
//可变的序列 import scala.collection.mutable._

object ListDemoMu {
  def main(args: Array[String]): Unit = {
    //构建一个可变序列
    val list1 = ListBuffer[Int](1,2,3)
    //创建一个空的可变序列
    val list2 = new ListBuffer[Int]
    println(list1+"\n"+list2)
    //向list2中追加元素,此时并没有生成新的集合
    list2 += 4
    println(list2)
    
    //将list2的元素追加到list1,此时未生成新集合
    list1 ++= list2
    println(list1)
    //将list1与list2合并,此时生成新集合
    val list3 = list1 ++ list2
    println(list3)
    //将元素追加到list1后面生成一个新的集合
    val list4 = list1 :+ 5
    println(list4)
    
  }
}