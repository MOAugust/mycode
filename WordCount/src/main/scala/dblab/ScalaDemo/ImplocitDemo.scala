package dblab.ScalaDemo


class Person(val name: String) {
  def sayHello: Unit = {
    println("Hello, my name is "+ name)
  }
  //2个人交朋友
  def mkFriends(p: Person): Unit = {
    sayHello
    p.sayHello
  }
}

class Student(name: String) extends Person(name)
class Dog(val name: String)

//聚会时两人交朋友
//视图上边界:[T <% Person] ：标识这个类型必须是Person 或者Person子类
//此外还有视图下边界:即指定泛型类型必须是某个类的父类
class Party[T <% Person](p1: Person,p2: Person) {
  p1.mkFriends(p2)
}


object ImplocitDemo {
  //隐式转换,将狗转为人
  implicit def dog2Person(dog: Dog): Person = {
    new Person(dog.name)
  }
  
  def main(args: Array[String]): Unit = {
   val zg = new Person("zg")
   val mm = new Student("mm")
   new Party[Person](zg,mm)
   println("----------------------------------------------")
   
   val ren = new Person("mzg")
   val gou = new Dog("huahua")
   new Party[Person](ren,gou)
  }
}