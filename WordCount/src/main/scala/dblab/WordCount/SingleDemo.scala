package dblab.WordCount

import scala.collection.mutable.ArrayBuffer

object SingleDemo {
  def main(args: Array[String]): Unit = {
    val session = SessionFactory.getSession()
    println("?"+session)
    
  }
}
class Session{
  print(SessionFactory.counts)
  println("----------")
  
}

object SessionFactory{
  //该部分相当于java静态块
  var counts = 5
  val sessions = new ArrayBuffer[Session]()
  while (counts > 0){
    sessions += new Session
    counts -= 1
  }
  def getSession(): Session = {
    sessions.remove(0)
  }
}
 
