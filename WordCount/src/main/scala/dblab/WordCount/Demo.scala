package dblab.WordCount


class Inner(val name:String,val age:Int){
  //主构造器会执行类定义中的所有语句
  println("Hello Scala")
  println(name+"\t"+age)
  val x = 1
  if(x > 1){
    println("大于1")
  }else if(x ==1){
    println("等于1")
  }else{
    println("不知")
  }
  
  private var address = "GS"
  //用this关键字定义辅助构造器
  def this(name:String,age:Int,address:String){
    //每个辅助构造器必须以主构造器或其他辅助构造器的调用开始
    this(name,age)
    println("执行辅助构造器")
    this.address = address
    println(name+"\t"+age+"\t"+address)
  }
 }
object Inner{
    def main(args: Array[String]): Unit = {
    val p = new Inner("MZG",27,"GD")
    
  }
}