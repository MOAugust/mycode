package dblab.WordCount

class bs {
  val id = 666
  private var name = "hello"
  def printName(): Unit ={
    //伴生对象,在伴生类(类名与对象名相同)访问伴生对象的私有属性co
    println(bs.co+"\t"+name)
  }
}
/**
 * 伴生对象
 */
object bs {
  //伴生对象私有属性
  private var co = "大大大"
  //主方法
  def main(args: Array[String]): Unit ={
  val bsn = new bs
  //访问私有字段name
  bsn.name = "hello Scala"
  bsn.printName()
  //调用伴生对象的apply方法
  val arr = Array(1,2,3,4)
  println(arr.toBuffer)
  //new了一个长度为9的数组，数组里面包含了9个null
  var ar = new Array(9)
  println(ar)
  
  }
}