package dblab.WordCount

//trait定义抽象方法
trait Demo {
  def prn(name: String)
  def prnt(name: String)
  def prntl(name: String){}
}
//trait定义具体方法
trait Demo1 {
  def log(message: String) = println(message)
}
//trait定义具体字段
trait Demo2 {
  val num: Int = 2
}
//trait定义抽象字段
trait Demo3 {
  val msg: String
  def d3(name: String) = println(msg+" "+name)
}
//实例对象混入trait
trait Demo4 extends Demo {
  override def prnt(name: String): Unit = {
    println("4我是"+name)
  }
}
trait Demo5 extends Demo {
  override def prnt(name: String): Unit = {
    println("5我是"+name)
  }
}
//trait调用链
trait Demo6 extends Demo {
  override def prntl(name: String): Unit = {
    println("6我是:"+name)
    super.prntl(name)
  }
}

trait Demo7 extends Demo {
  override def prntl(name: String): Unit = {
    println("7我是:"+name)
    super.prntl(name)
  }
}

class Per(val name: String) extends Demo1 
with Demo2 with Demo3 with Demo with Demo4 with Demo6 with Demo7 {
  def prn(name: String) = println("抽象方法实现:"+name)
  val msg: String = "hhh msg:"
  def mk(p: Per): Unit = {
    println("my name is :"+name+" ;other name is :"+p.name)
    log("we are friends")
    println("you say:" +num)
    d3(p.name)
    prnt("aa")
    prntl("链")
  }
}
object AbsTrait{
  def main(args: Array[String]): Unit = {
    val p1 = new Per("mzg")
    val p2 = new Per("mm")
    val p3 = new Per("zzz") with Demo5
    p1.mk(p2)
    p1.d3(p1.name)
    p1.prn(p2.name)
    p3.prnt("lllll")
  }
}