package dblab.WordCount

 import org.apache.spark.SparkContext
 import org.apache.spark.SparkContext._
 import org.apache.spark.SparkConf
 import org.apache.spark.rdd.RDD
 
object WordCount {
    def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    /**
      * 如果这个参数不设置，默认认为你运行的是集群模式
      * 如果设置成local代表运行的是local模式
      */
    conf.setMaster("local")
    //设置任务名
    conf.setAppName("Scala0701")
    //创建SparkCore的程序入口
    val sc = new SparkContext(conf)
    //读取文件 生成RDD
    val file: RDD[String] = sc.textFile("hdfs://192.168.220.132:9013/user/root/Scala0701.txt")
    //把每一行数据按照空格分割
    val word: RDD[String] = file.flatMap(_.split(" "))
    //让每一个单词都出现一次
    val wordOne: RDD[(String, Int)] = word.map((_,1))
    //单词计数
    val wordcount: RDD[(String, Int)] = wordOne.reduceByKey(_+_)
    //按照单词出现的次数即上个RDD中第二列排序 
    val sortRdd: RDD[(String, Int)] = wordcount.sortBy(x => x._2)
    //输出结果
    println("--------------------------")
    sortRdd.foreach(print)
    println("--------------------------")
    //保存结果
    sortRdd.saveAsTextFile("/home/result")
    sc.stop()
  }
}