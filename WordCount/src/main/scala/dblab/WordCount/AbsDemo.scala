package dblab.WordCount

abstract class FDemo(name:String) {
  def sayHello: Unit
}
trait HelloC {
  def sayNO(name: String)
}
trait HelloN {
  def sayName(a: AbsDemo)
}
class AbsDemo(var name:String) extends FDemo(name) with HelloC with HelloN{
  def sayHello: Unit = println(name + "Hello!")
  def sayNO(name: String) = println(name + "NO")
  def sayName(a: AbsDemo) = println(name + "--" + a.name)
}
object AbsDemo {
  def main(args: Array[String]): Unit = {
    var i = new AbsDemo("zg")
    var j = new AbsDemo("mzg")
    i.sayHello
    i.sayNO("mm")
    i.sayName(j)
    println("-----")
    i.sayName(i)
  }
  
}